# sqlfy.py

import sys
import jinja2 as j2

args = sys.argv[1:]

templateLoader = j2.FileSystemLoader(searchpath = '.')
env = j2.Environment(loader = templateLoader, undefined=j2.DebugUndefined)

template = env.get_template(args[0])

template_args = [
#	table, audit?, soft-delete?, auto_id?, [fk]
	('DATA_SOURCE', False, False, True, []),
	('IMPORT_SET', False, False, True, []),
	('FORMAT', False, False, True, []),
	('OPERATOR', False, False, True, []),
	('MEDIATYPE', False, False, True, []),
	('MEDIACATEGORY', False, False, True, [('MEDIATYPE_ID', 'MEDIATYPE', 'ID')]),
	
	('INVENTORY', True, True, [
		#('fk_colname', 'fk_table', 'fk_id'),
		('DATA_SOURCE_ID', 'DATA_SOURCE', 'ID'),
		('FORMAT_ID', 'FORMAT', 'ID'),
		('IMPORT_SET_ID', 'IMPORT_SET', 'ID'),
		('OPERATOR_ID', 'OPERATOR', 'ID'),
		('MEDIATYPE_ID', 'MEDIATYPE', 'ID'),
		('LOCATIONKEY', 'LOCATION', 'LOCATIONKEY')
		])
	]




for t in template_args:

	xyz = {
		'tablename': t[0],
		'audit' : t[1],
		'soft_delete': t[2],
		'auto_id': t[3],
		'foreign_keys': t[-1],
		}
	
	print('-- '+t[0])
	print(template.render(xyz))


