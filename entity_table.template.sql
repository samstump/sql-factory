{% import 'sqldef.j2' as sqldef -%}

{# Entity Table:
    supports:
        - meaningful autoinc ids
        - multiple soft deleted versions of rows
        - insert and update audit tracking
-#}

CREATE TABLE {{tablename}} (
    {% if auto_id -%}
        {{ sqldef.auto_inc('ID') | indent }},
    {% endif %}   
    {% if row_soft_delete -%}
        {{ sqldef.soft_delete() | indent }},
    {% endif %}
    {{ sqldef.simple_name_descr() | indent}},      
    
    {{ sqldef.declare_pk('ID') | indent }},
    
    {% for row in foreign_keys -%}
        {{ sqldef.declare_fk(row[0], row[1], row[2]) | indent}},
        
    {% endfor -%}
    {% if audit -%}
        {{ sqldef.audit_columns() | indent}}
    {% endif %}
);

{% if soft_delete -%}
-- soft delete trigger
CREATE TRIGGER {{tablename}}_SOFT_DELETE
BEFORE DELETE on {{tablename}} FOR EACH ROW SET
    NEW.DELETED = 
        CASE
            WHEN OLD.DELETED != 0 THEN OLD.DELETED
            ELSE OLD.ID
        END;
    SIGNAL 'row soft-deleted'
END;
{% endif -%}

{% if audit -%}
-- audit triggers
CREATE TRIGGER {{tablename}}_AUDIT_INSERT
BEFORE INSERT on {{tablename}} FOR EACH ROW SET
  NEW.CREATED_TS = {{sqldef.db_user()}},
  NEW.CREATED_BY = {{sqldef.db_now()}},
  NEW.UPDATED_TS = {{sqldef.db_user()}},
  NEW.UPDATED_BY = {{sqldef.db_now()}};

CREATE TRIGGER {{tablename}}_AUDIT_INSERT
BEFORE UPDATE on {{tablename}} FOR EACH ROW SET
    NEW.UPDATED_TS = {{sqldef.db_user()}},
    NEW.UPDATED_BY = {{sqldef.db_now()}};
{% endif -%}

