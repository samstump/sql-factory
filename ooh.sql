/*
	Entity Table:
	supports:
		- meaningful autoinc ids
		- multiple soft deleted versions of rows
		- insert and update audit tracking
*/
CREATE TABLE <tablename> (
	-- identity and deleted, note: same type
	ID INT AUTO_INCREMENT,
	DELETED INT DEFAULT 0,
	
	-- pk
	PRIMARY KEY(ID, DELETED),
	
	-- fk and references
	GAMMA_ID INT,
	ZAPHL_ID INT,
	BETA_ID INT,
	FOREIGN KEY GAMMA_ID REFERENCES GAMMA(ID),
	FOREIGN KEY ZAPHL_ID REFERENCES ZAPHL(ID),
	FOREIGN KEY BETA_ID REFERENCES BETA(ID),

	-- audit fields
	UPDATED_TS DATETIME,
	UPDATED_BY VARCHAR(128),
	CREATED_TS DATETIME,
	CREATED_BY VARCHAR(128)
);

-- soft delete trigger
CREATE TRIGGER <tablename>_SOFT_DELETE
BEFORE DELETE on <tablename> FOR EACH ROW
BEGIN
	IF NEW.DELETED = 0 THEN
		NEW.DELETED = OLD.ID;
	END IF;
	SIGNAL 'row soft-deleted'
END;

-- audit triggers
CREATE TRIGGER <tablename>_AUDIT_INSERT
BEFORE INSERT on <tablename> FOR EACH ROW
BEGIN
	SET NEW.CREATED_TS = NOW(),
	SET NEW.CREATED_BY = CURRENTUSER(),
	SET NEW.UPDATED_TS = NOW(),
	SET NEW.UPDATED_BY = CURRENTUSER()
END;

CREATE TRIGGER <tablename>_AUDIT_INSERT
BEFORE UPDATE on <tablename> FOR EACH ROW
BEGIN
	SET NEW.UPDATED_TS = NOW(),
	SET NEW.UPDATED_BY = CURRENTUSER()
END;


/*
	standard relation table
	supports:
		- no meaningful autoinc ids -- these are relations (gamma, beta).
		- multiple soft deleted versions of rows
		- insert and update audit tracking
*/
CREATE TABLE <tablename> (
	-- identity and deleted, note: same type
	XID INT AUTO_INCREMENT,
	DELETED INT DEFAULT 0,
	
	-- relates two FKs
	GAMMA_ID INT,
	BETA_ID INT,
	FOREIGN KEY GAMMA_ID REFERENCES GAMMA(ID),
	FOREIGN KEY BETA_ID REFERENCES BETA(ID),

	-- pk
	PRIMARY KEY(GAMMA_ID, BETA_ID, DELETED),
	
	-- audit fields
	UPDATED_TS DATETIME,
	UPDATED_BY VARCHAR(128),
	CREATED_TS DATETIME,
	CREATED_BY VARCHAR(128)
	
);

-- soft delete trigger
CREATE TRIGGER <tablename>_SOFT_DELETE
BEFORE DELETE on <tablename> FOR EACH ROW
BEGIN
	IF NEW.DELETED = 0 THEN
		NEW.DELETED = OLD.ID
	END IF;
	SIGNAL 'row soft-deleted'
END;

-- audit triggers
CREATE TRIGGER <tablename>_AUDIT_INSERT
BEFORE INSERT on <tablename> FOR EACH ROW
BEGIN
	SET NEW.CREATED_TS = NOW(),
	SET NEW.CREATED_BY = CURRENTUSER(),
	SET NEW.UPDATED_TS = NOW(),
	SET NEW.UPDATED_BY = CURRENTUSER()
END;

CREATE TRIGGER <tablename>_AUDIT_INSERT
BEFORE UPDATE on <tablename> FOR EACH ROW
BEGIN
	SET NEW.UPDATED_TS = NOW(),
	SET NEW.UPDATED_BY = CURRENTUSER()
END;
